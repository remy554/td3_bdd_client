package com.example.remy5.bdd_td3

import org.jetbrains.anko.db.*

class CourseDb(private val courseDbHelper: CourseDbHelper = CourseDbHelper.instance) {

    fun requestCourse() = courseDbHelper.use {
        select("CourseTable",
                CourseDbHelper.CourseTable.ID,
                CourseDbHelper.CourseTable.title,
                CourseDbHelper.CourseTable.cover)
                .parseList(classParser<Course>())
    }

    fun saveCourse(id:String, title: String, cover: String) = courseDbHelper.use {
        insert("CourseTable",
                CourseDbHelper.CourseTable.ID to id,
                CourseDbHelper.CourseTable.title to title,
                CourseDbHelper.CourseTable.cover to cover)
    }
}
