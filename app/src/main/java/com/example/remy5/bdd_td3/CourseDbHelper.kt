package com.example.remy5.bdd_td3

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

class CourseDbHelper(ctx: Context = AppInstance.instance) : ManagedSQLiteOpenHelper(ctx,
        DB_NAME, null, DB_VERSION) {


    companion object DbObject {
        val DB_NAME = "forecast.db"
        val DB_VERSION = 1
        val instance by lazy { CourseDbHelper() }
    }

    object CourseTable {
        val title = "title"
        val cover = "cover"
        val ID = "id"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable("CourseTable",true,
                CourseTable.ID to TEXT + PRIMARY_KEY,
                        CourseTable.title to TEXT,
                        CourseTable.cover to TEXT)
    }


    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable("CourseTable", false)
        onUpgrade(db, oldVersion, newVersion)
    }

}