package com.example.remy5.bdd_td3

import android.app.Application

/**
 * Created by remy5 on 16/12/2017.
 */
class AppInstance : Application() {

    companion object {
        lateinit var instance: AppInstance
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

}
