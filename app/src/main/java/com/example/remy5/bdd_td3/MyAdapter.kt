package com.example.remy5.bdd_td3

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

import java.util.ArrayList
import java.util.Arrays


class MyAdapter(private val courses: List<Course>, private val context: Context) : RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

    override fun getItemCount(): Int = courses.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.list_cell, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val course = courses[position]
        holder.display(course)
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val name: TextView = itemView.findViewById<View>(R.id.name) as TextView
        private val cover: ImageView = itemView.findViewById<View>(R.id.cover) as ImageView

        private var currentCourse: Course? = null

        init {
            // Item click
            itemView.setOnClickListener {
                AlertDialog.Builder(itemView.context)
                        .setTitle(currentCourse!!.title)
                        .setMessage("ID : " + currentCourse!!.id)
                        .show()
            }

        }

        fun display(c: Course) {
            currentCourse = c
            name.text = c.title
            Picasso.with(context)
                    .load(c.cover)
                    .placeholder(R.drawable.ic_launcher_background)
                    .error(R.drawable.ic_launcher_background)
                    .into(cover);
        }
    }

}