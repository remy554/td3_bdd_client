package com.example.remy5.bdd_td3

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


class MainActivity : AppCompatActivity() {
    // Attributes
    private var coursesDb:CourseDb? = null
    private var listeCourses:List<Course>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Get courses
        coursesDb = CourseDb()
        listeCourses = coursesDb?.requestCourse()

        // Recycler View
        list.layoutManager = LinearLayoutManager(this)
        list.adapter = MyAdapter(listeCourses!!, this)

        getCourses()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        val inflater = menuInflater
        inflater.inflate(R.menu.my_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if(item?.itemId == R.id.action_refresh){
            getCourses()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun getCourses(){
        try {
            val url = "http://mobile-courses-server.herokuapp.com/"

            val retrofit = Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(MoshiConverterFactory.create())
                    .build()

            val service = retrofit.create(CoursesService::class.java)
            val courseRequest = service.listCourses()

            courseRequest.enqueue(object : Callback<List<Course>> {
                override fun onResponse(call: Call<List<Course>>, response: Response<List<Course>>) {
                    val allCourse = response.body()
                    if (allCourse != null) {
                        // Get courses
                        for (course in allCourse) {
                            val id = course.id
                            val title = course.title
                            val cover = course.cover

                            if (!existInList(id)) {
                                // Add to list
                                (listeCourses as ArrayList<Course>).add(course)

                                // Save in DB
                                coursesDb!!.saveCourse(id, title, cover)
                            }
                        }

                        // Refresh UI
                        list.adapter.notifyDataSetChanged()
                    }
                }

                override fun onFailure(call: Call<List<Course>>, t: Throwable) {
                    toast("Impossible de se connecter à internet !")
                }
            })

        }catch (e: Exception){
            print(e)
        }
    }

    private fun existInList(id: String): Boolean = listeCourses!!.any { it.id == id }

}
