package com.example.remy5.bdd_td3;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CoursesService {

    @GET("/courses")
    Call<List<Course>> listCourses();

}